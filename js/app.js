/**
* gem store, sample code for angular
* app.js
* Pello Altadill - http://pello.io
*/
(function () {

var app = angular.module('store',['store-products']);

app.controller('StoreController', [ '$http',function ($http) {
		var store = this;
		store.products = []; // we initialize to avoid the page to look wierd while loading

		$http.get('/products.json').success(function (data){
			store.products = data;
		});
		/**
		*
			$http.post('/path/to/resource.json', {param: 'value'})
			$http.delete('/path/to/resource.json')
			// other http methods using config object
			$http({method: 'OPTIONS', url: '/path/to/resource.json'});
			$http({method: 'PATCH', url: '/path/to/resource.json'});
			$http({method: 'TRACE', url: '/path/to/resource.json'});
		*/

	]
});

app.controller('PanelController', function () {
	this.tab = 1;
	this.selectTab = function (setTab) {
		this.tab = setTab;
	};

	this.isSelected = function (checkTab) {
		return this.tab = checkTab;
	};
});

app.controller('ReviewController', function () {
	this.review = {};
	this.addReview= function (product) {
		product.reviews.push(this.review);
		this.review = {};
		// clear out the form, so the form will hide values
	};

/**
* Services give functionality like
* 1. Fetching JSON data from a web service with $http
* 2. Logging messages to the javascript console with $log
* 3. Filtering an array with $filter
Builting services from angular: $

To fetch data

$http({ method: 'GET', url: '/products.json'})
or
$http.get('/products.json'.{apiKey: 'myApiKey'});
both return promises, allowing us to use callback: success or error
* how to use this in a controller
// where $httpsd is service name, and the parameter in function is
// dependency injection
app.controller('SomeController', ['$http', function ($http) {
	
}])
// You need more?
app.controller('SomeController', ['$http','$log', function ($http,'$log') {
	
}])
Angular has an injector with registered things, as $http, $log
when some controller needs something,  they're injected

*/



var gems = [
	{
		name: 'Dodecahedron',
		price: 2.95,
		description: 'The Pythagorean dodecahedron',
		canPurchase: false,
		images [{
			full: 'img/dode.jpg',
			thumb: 'img/dodet.jpg',
		}],
		reviews: [
			{
				stars: 5,
				body: "I love this",
				author: "joe@thomas.com"
			},
			{
				stars: 1,
				body: "This sux, I rule",
				author: "joe@hater.com"
			}
			]
			}
		} 
	];

})();