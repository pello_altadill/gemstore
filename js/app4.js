/**
* gem store, sample code for angular
* app.js
* Pello Altadill - http://pello.io
*/
(function () {

var app = angular.module('store',[]);

app.controller('StoreController', function () {
	this.product = gems;
});

app.controller('PanelController', function () {
	this.tab = 1;
	this.selectTab = function (setTab) {
		this.tab = setTab;
	};

	this.isSelected = function (checkTab) {
		return this.tab = checkTab;
	};
});

app.controller('ReviewController', function () {
	this.review = {};
	this.addReview= function (product) {
		product.reviews.push(this.review);
		this.review = {};
		// clear out the form, so the form will hide values
	};

// <product-title> must be translated to camelCase
app.directive('productTitle',function () {
	return {
		restrict: 'E', // stands for html elementys
		templateUrl: 'product-title.html' // url for a template
	};
	/* 
	// attribute directive is just to use A
	return {
		restrict: 'A', // stands for html attribute
		templateUrl: 'product-title.html' // url for a template
	};*/
});

// We have to move the product panel controller here:
// Use the ALIAS and move the functionality to the controller data:
app.directive( 'productPanels', function () {
	return {
		restrict: 'E',
		templateUrl: 'product-panels.html',
		controller:function () {
			this.tab = 1;
			this.selectTab = function (setTab) {
				this.tab = setTab;
			};

			this.isSelected = function (checkTab) {
				return this.tab = checkTab;
			};
		},
		controllerAs: 'panels'

	};

});

var gems = [
	{
		name: 'Dodecahedron',
		price: 2.95,
		description: 'The Pythagorean dodecahedron',
		canPurchase: false,
		images [{
			full: 'img/dode.jpg',
			thumb: 'img/dodet.jpg',
		}],
		reviews: [
			{
				stars: 5,
				body: "I love this",
				author: "joe@thomas.com"
			},
			{
				stars: 1,
				body: "This sux, I rule",
				author: "joe@hater.com"
			}
			]
			}
		} 
	];

})();