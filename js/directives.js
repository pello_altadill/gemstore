// We use a different closure, which means that app is another variable!!!!
(function () {

var app = angular.module('store-products',[]);

// <product-title> must be translated to camelCase
app.directive('productTitle',function () {
	return {
		restrict: 'E', // stands for html elementys
		templateUrl: 'product-title.html' // url for a template
	};
	/* 
	// attribute directive is just to use A
	return {
		restrict: 'A', // stands for html attribute
		templateUrl: 'product-title.html' // url for a template
	};*/
});

// We have to move the product panel controller here:
// Use the ALIAS and move the functionality to the controller data:
app.directive( 'productPanels', function () {
	return {
		restrict: 'E',
		templateUrl: 'product-panels.html',
		controller:function () {
			this.tab = 1;
			this.selectTab = function (setTab) {
				this.tab = setTab;
			};

			this.isSelected = function (checkTab) {
				return this.tab = checkTab;
			};
		},
		controllerAs: 'panels'

	};

});


})();